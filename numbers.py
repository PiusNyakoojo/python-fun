

# Imaginary numbers

x = 12j + 1

print(x)
print(type(x))

# Modulus operator

a = 5 / 3
b = 5 % 3

print("a is %d" % a)
print("b is %d" % b)

# Formatting Numbers as Octal and Hexadecimal

print("Octal uses the letter 'o' lowercase. %d %o" % (10, 10))
print("Hex uses the letter 'x' or 'X'. %d %x %X" % (10, 10, 61))
