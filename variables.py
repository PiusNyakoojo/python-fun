
# Tuples - Unchanging Sequences of Data

print ("A %s %s %s %s" % ("string", "filled", "by a", "tuple"))

a = (1, 2, 3, 4, (1, 2, 3), (4, 5, 6))

print ("Tuple acceess: %s" % a[4][0])

# Sequences - Changeable Sequences of Data

breakfast = ["coffee", "tea", "toast", "egg"]

breakfast.append("sausages")
breakfast.extend(["juice", "decaf", "oatmeal"])

# Dictionaries - Groupings of Data Indexed by Name
'''
A dictionary is similar to lists and tuples. It's another
type of container for a group of data. However, whereas tuples
and lists are indexed by their numeric order, dictionaries
are indexed by names that you choose. These names can be
letters, numbers, strings or symbols - whatever suits you.
'''

menus_specials = {}
menus_specials["breakfast"] = "canadian ham"
menus_specials["lunch"] = "tuna surprise"
menus_specials["dinner"] = "Cheeseburger Deluxe"

# Getting the keys from a dictionary
menus_specials.keys()

# Getting the values from a dictionary
menus_specials.values()

# Checking if key already exists
menus_specials.__contains__("lunch") # True or False
