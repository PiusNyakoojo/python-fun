from PIL import Image
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np

img = Image.open('./images/stinkbug.png')

# Resizes an image in place
img.thumbnail((64, 64), Image.ANTIALIAS)

# Pixelated
plt.figure(0)
f0 = plt.imshow(img)

# Nearest-neighbor interpolation
plt.figure(1)
f1 = plt.imshow(img, interpolation="nearest")

# People tend to prefer blurry over pixelated
plt.figure(2)
plt.imshow(img, interpolation="bicubic")

# Let's look at the difference between Pixelated and nearest-neighbor

arr0 = f0.get_array()
arr1 = f1.get_array()

diff = 10 * (arr1 - arr0)

plt.figure(3)
plt.imshow(diff)

# 0 for this example.. just making sure :)
print("diff between pixelated and nearest-neighbors: %d" % np.sum(diff))

plt.show()
