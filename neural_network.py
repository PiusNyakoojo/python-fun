import numpy as np

# Sigmoid function (run on every neuron of our network)
def nonlin(x, deriv=False):
    if (deriv == True):
        return x * (1 - x)

    return 1/(1 + np.exp(-x))

# input data
# Each column represents a different neuron
X = np.array([[0, 0, 1],
            [0, 1, 1],
            [1, 0, 1],
            [1, 1, 1]])

# output data
y = np.array([[0],
            [1],
            [1],
            [0]])

# Make random number generation deterministic - useful for debugging
np.random.seed(1)

# Synapses are the connections from all the neurons in one layer
# To every other neuron in the next layer.
# Since we have 3 layers (input, 1 hidden and output) we need
# 2 synapse matrices.
# Each synapse has a random weight assigned to it

syn0 = 2 * np.random.random((3, 4)) - 1
syn1 = 2 * np.random.random((4, 1)) - 1

# Now comes the prediction step
for j in range(60000):

    # 3 layers of the network
    # Notice that layer 1 contains the input neurons as well as the amount of data
    # The performance of the network is deeply tied to the amount of data available
    # X is a 3 x 4 matrix. 3 for the number of "features" or "inputs" we are observing
    # 4 is for the amount of data we have available
    l0 = X

    # layer 1 is the result of a dot product between the input data and an initially randomized
    # matrix that's meant to extract some useful information about the input as we increase
    # the training iterations of our network. In other words, the results of layer 1 (our hidden layer)
    # depend heavily on the number of iterations that we run our network. The more iterations
    # we train our network,
    l1 = nonlin(np.dot(l0, syn0))
    l2 = nonlin(np.dot(l1, syn1))

    # This is where our input data touches our expected output. As a supervised approach, we need to
    # provide a reference as to what we expect the system should do. We can use the system's Output
    # with our expectation to give feedback to the system and direct it towards our expectation
    l2_error = y - l2

    if (j % 10000) == 0:
        print("Error:" + str(np.mean(np.abs(l2_error))))

    l2_delta = l2_error * nonlin(l2, deriv=True)

    l1_error = l2_delta.dot(syn1.T)

    l1_delta = l1_error * nonlin(l1, deriv=True)

    # Update weights
    syn1 += l1.T.dot(l2_delta)
    syn0 += l0.T.dot(l1_delta)

print("Output after training")
print(l2)
