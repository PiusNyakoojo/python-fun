import numpy as np
import matplotlib.pyplot as plt

N = 8
y = np.zeros(8)
x1 = np.linspace(0, 3, num=N, endpoint=True)
x2 = np.linspace(0, 3, num=N, endpoint=False)

plt.plot(x1, y, 'o')
plt.plot(x2, y + 0.5, 'o')
plt.ylim([-0.5, 1])

plt.show()
