import numpy as np
import matplotlib.pyplot as plt

N = 10 # size
noise_std = 0.1 #

x1 = np.random.normal(0, noise_std, size=N)
y = np.linspace(0, N, num=N)

plt.plot(y, x1, 'o')
plt.ylim([-0.5, 2])

print(x1)
plt.show()
