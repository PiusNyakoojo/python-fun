import tensorflow as tf

# variables allow us to add trainable parameters to a graph.
# they are constructed with a type and initial value.

W = tf.Variable([0.3], dtype=tf.float32)
b = tf.Variable([-0.3], dtype=tf.float32)
x = tf.placeholder(tf.float32)

linear_model = W * x + b

# init is a handle to the tensorflow sub-graph that initializes all global variables.
# until we call sess.run, the variables are uninitialized
init = tf.global_variables_initializer()

sess = tf.Session()
sess.run(init)

# since x is a placeholder, we can evaluate linear_model for several values of x
# print(sess.run(linear_model, { x: [1, 2, 3, 4, 5, 6]}))


# we don't know how good our model is until we test it on some data.
# let's create a vector y which will represent the desired values for our data set.
# we can write a function to let us know how good our model is with respect to our
# desired results.
# there are various ways in writing such a "loss" function.
# a common way is to take the sum of the squares of the deltas between our model's
# prediction and the expected results

y = tf.placeholder(tf.float32)
squared_deltas = tf.square(linear_model - y) # recall the delta of values a and b is |b - a|
loss = tf.reduce_sum(squared_deltas)

# suppose our data set contains the points: (1, 0), (2, -1), (3, -2), (4, -3)
# running this data set against our linear model of 0.3 * x - 0.3 results in a fairly high loss.
# in other words, our linear model isn't very good at predicting what values we would get for
# an arbitrary x.
# print(sess.run(loss, { x: [1, 2, 3, 4], y: [0, -1, -2, -3]}))

# suppose our data set contains the points: (1, 0), (2, 0.3), (3, 0.6), (4, 0.9)
# running this data set against our linear model of 0.3 * x - 0.3 results in a fairly small loss.
# in fact our loss value is very close to zero compared to the previous result.
# this suggests we can have confidence in our model for predicting future values of y when we
# have an x
# lower loss
# print(sess.run(loss, { x: [1, 2, 3, 4], y: [0, 0.3, 0.6, 0.9]}))

# we can manually improve our model on the first data set: (1, 0), (2, -1), (3, -2), (4, -3)

fixW = tf.assign(W, [-1])
fixb = tf.assign(b, [1])

updated_linear_model = fixW * x + fixb

updated_sq_deltas = tf.square(updated_linear_model - y)
updated_loss = tf.reduce_sum(updated_sq_deltas)

print(sess.run(updated_loss, { x: [1, 2, 3, 4], y: [0, -1, -2, -3]}))
