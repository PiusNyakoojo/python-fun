import tensorflow as tf

# constants

node1 = tf.constant(3.0, dtype=tf.float32)
node2 = tf.constant(4.0) # also a tf.float32 implicitly

node3 = tf.add(node1, node2)

sess = tf.Session()
sess2 = tf.Session()

node4 = tf.add(node1, node1)

'''
print(sess2.run(node4))
print(sess.run(node3))
print(sess.run(node4))
'''

# placeholders
# placeholders are promises to provide values later

a = tf.placeholder(tf.float32)
b = tf.placeholder(tf.float32)

adder_node = tf.add(a, b) # or we can write a + b which a a short-hand method for tf.add(a, b)

'''
print(sess.run(adder_node, { a: 3, b: 4.5 }))
print(sess.run(adder_node, { a: [1, 2], b: [3, 4]}))
'''

square_add = adder_node * adder_node

print(sess.run(square_add, { a: 3, b: 4 }))
print(sess.run(square_add, { a: [1, 2], b: [1, 2]}))
