import tensorflow as tf

W = tf.Variable([1.], tf.float32)
b = tf.Variable([-1.], tf.float32)
x = tf.placeholder(tf.float32)

linear_model = W * x + b

init = tf.global_variables_initializer()

sess = tf.Session()

sess.run(init) # initialize variables

# setup loss function
y = tf.placeholder(tf.float32)
sq_dt = tf.square(linear_model - y)
loss = tf.reduce_sum(sq_dt)

# we will use gradient descent to automatically update our variables in
# order to minimize our loss function
optimizer = tf.train.GradientDescentOptimizer(0.01)
train = optimizer.minimize(loss)

# setup data set
data_set = {
    x: [1, 2, 3, 4],
    y: [0, -1, -2, -3]
}

# train for multiple iterations
for i in range(1000):
    sess.run(train, data_set)
    # print("loss: ", sess.run(loss, data_set))

# print the resulting weight and bias values
print("W, b:", sess.run([W, b]))
