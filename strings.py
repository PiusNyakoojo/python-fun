

print ("%s %s %f" % ("Hello, ", "World: ", 3.1415927))

# Escaping the % sign in strings

print("The %% behaves differently when combined" +
    "with other letters like %%d %%s %%f %d" % (10))
