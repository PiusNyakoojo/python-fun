import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np

img = mpimg.imread('images/stinkbug.png')

# Dimensions of image width x length x depth (rgba)
print(img.shape)

# Number of elements in image
print(img.size)

# Shape of image to count number of elements
print(375 * 500 * 3)


imgplot = plt.imshow(img)

# Show the plot
plt.show()
